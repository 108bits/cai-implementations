program cifar_trainer;

{$MODE OBJFPC}{$H+}
{$MODESWITCH ADVANCEDRECORDS}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp, MTProcs, Math,
  { you can add units after this }
  UCIFAR10, UConvolutionNeuralNetwork, UVolume
  {$IFDEF OpenCL}
  , UEasyOpenCL, CL
  {$ENDIF}
  ;

type
  { TTinyImagesArray }
  TTinyImagesArray = Array of TTinyImage;

  { TNeuralNetworkConfig }
  TNeuralNetworkConfig = record
    Width: Integer;
    Height: Integer;
    Depth: Integer;
		FeaturesNum: Integer;
    FeatureSize: Integer;
    Stride: Integer;
    MaxPool: Integer;
    NumClasses: Integer;

    LogicalThreadsNum: Integer;
    PhysicalThreadsNum: Integer;
    BatchSize: Integer;

    Dropout: Single;
    LearningRate: Single;
    Inertia: Single;
    MinLearningRate: Single;
    LearningRateDecay: Single;

    StaircaseLearning: Boolean;
    StaircaseEpochs: Integer;

    NoiseLevel: Single;

    L2Decay: Single;

    ApplyL2DecayToAllLayers: Boolean;

    InnerConvNeuronCount: Integer;
    InnerConvFeatureSize: Integer;
    InnerConvPadding: Integer;
    InnerFullyConnectedNeurons: Integer;

    procedure SetDefaults;
  end;

  { TCifarTrainer }
  TCifarTrainer = class(TCustomApplication)
  private
  	FImagesLearn: TTinyImagesArray;
    FImagesValidate: TTinyImagesArray;
    FImagesTest: TTinyImagesArray;

    FImageVolumes: TNNetVolumeList;
    FValidationVolumes: TNNetVolumeList;
    FTestVolumes: TNNetVolumeList;
    FWorkingVolumes: TNNetVolumeList;

    FNN: TNNet;
    FNNConfig: TNeuralNetworkConfig;

{$IFDEF OpenCL}
    FUseOpenCL: Boolean;
    FOpenCL: TEasyOpenCLV;
{$ENDIF}

    FThreadNN: TNNetDataParallelism;
    FCritSect: TRTLCriticalSection;

    FGlobalHits: Integer;
    FGlobalMisses: Integer;
    FGlobalErrorSum: Single;
    FGlobalTotalLoss: Single;

    FDoAugmentation: Boolean;
    FIsNewNetwork: Boolean;

    procedure Train(AEpochs: Integer; ATargetError: Single);

    function Prepare: Boolean;
    procedure Finish;

    procedure OpenCL_Probe;

    procedure LoadImagesInto(AFileName: String; var ATarget: TTinyImagesArray);
    procedure LoadImages;
    procedure LoadIntoNNetVolume(AList: TNNetVolumeList; AImages: TTinyImagesArray);
    procedure FreeImages;

    procedure ModifyInput(var AInput: TNNetVolume);

    procedure ThreadTrainProc(Index: PtrInt; Data: Pointer;
      Item: TMultiThreadProcItem);
    procedure ThreadTestProc(Index: PtrInt; Data: Pointer;
      Item: TMultiThreadProcItem);

    function CreateNeuralNetwork: Boolean;
    procedure FreeNeuralNetwork;
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TNeuralNetworkConfig }

// Set Neural Network configuration defaults
procedure TNeuralNetworkConfig.SetDefaults;
begin
  Width := 32;
  Height := 32;
  Depth := 3;
	FeaturesNum := 32;
  FeatureSize := 5;
  Stride := 1;
  MaxPool := 4;
  NumClasses := 10;

  LogicalThreadsNum := 4;
  PhysicalThreadsNum := 4;
  BatchSize := 128;

  Dropout := 0.0;
  LearningRate := 0.001;
  Inertia := 0.99;

  MinLearningRate := 0.00001;
  LearningRateDecay := 0.01;

  StaircaseLearning := TRUE;
  StaircaseEpochs := 50;

  NoiseLevel := 0.1;

  L2Decay := 0.001;
  ApplyL2DecayToAllLayers := TRUE;

  InnerConvNeuronCount := 64;
  InnerConvFeatureSize := 3;
  InnerConvPadding := 1;
  InnerFullyConnectedNeurons := 32;
end;

{ TCifarTrainer }

// Main Training, Validation and Test routine
procedure TCifarTrainer.Train(AEpochs: Integer; ATargetError: Single);
var
	i, Epoch, mainSeconds, epochSeconds, learningRateCounter: Integer;
  mainStartTime, epochStartTime, epochRunTime: Double;
  currentRate, currentLearningRate, newLearningRate,
  cvsTrainingRate, cvsTrainingLoss, cvsTrainingError,
  cvsValidationRate, cvsValidationLoss, cvsValidationError,
  cvsTestRate, cvsTestLoss, cvsTestError: Single;
  csvFile: TextFile;
  protocolFileName: String;
begin
  ForceDirectories('./backup');
  protocolFileName := './backup/protocol.csv';
  AssignFile(csvFile, protocolFileName);
  if FIsNewNetwork then
    begin
      Rewrite(csvFile);
      WriteLn(csvFile,
        'Epoch;Time;Learning Rate;',
        'Training Accuracy;Training Loss;Training Error;',
        'Validation Accuracy;Validation Loss;Validation Error;',
        'Test Accuracy;Test Loss;Test Error'
      );
    end
  else
    Append(csvFile);

  FDoAugmentation := FALSE;

  currentLearningRate := FNNConfig.LearningRate;
  learningRateCounter := 0;
  currentRate := 0;
  cvsTrainingRate := 0;
  cvsTrainingError := 0;
  cvsTrainingLoss := 0;
  cvsValidationRate := 0;
  cvsValidationLoss := 0;
  cvsValidationError := 0;
  cvsTestRate := 0;
  cvsTestLoss := 0;
  cvsTestError := 0;

  mainStartTime := Now;

  for Epoch := 1 to AEpochs do
    begin
			epochStartTime := Now;

      // ---- Learn ------------------------------------------------------------
      WriteLn('**** Learning ...');
      for i := 1 to FImageVolumes.Count div FNNConfig.BatchSize do
        begin
          FGlobalErrorSum := 0;
          FGlobalTotalLoss := 0;
          FGlobalHits := 0;
          FGlobalMisses := 0;

          currentRate := 0;
          cvsTrainingRate := 0;
          cvsTrainingError := 0;
          cvsTrainingLoss := 0;

          FNN.ClearTime();
          ProcThreadPool.DoParallel(@ThreadTrainProc, 0, FThreadNN.Count - 1,
            nil, FThreadNN.Count);
          FNN.UpdateWeights();
          FNN.ComputeL2Decay();

          if (FGlobalHits > 0) then
            begin
              currentRate := FGlobalHits / (FGlobalHits + FGlobalMisses);
              cvsTrainingError := FGlobalErrorSum / (FGlobalHits + FGlobalMisses);
              cvsTrainingLoss := FGlobalTotalLoss / (FGlobalHits + FGlobalMisses);
              cvsTrainingRate := currentRate;

              if cvsValidationRate > 0 then
                begin
                  // Enable augmentation if the NN is more than 10% overfit.
                  FDoAugmentation := cvsTrainingRate > cvsValidationRate + 0.1;
                end;
            end;

          // Output some statistics every 10 steps
          if (FGlobalHits > 0) and (i mod 10 = 0) then
            begin
              epochRunTime := (Now - epochStartTime) * 24 * 60 * 60;

              WriteLn(
                Epoch:2, i:6,
                ' Accuracy: ', (cvsTrainingRate * 100):5:2, '% ',
                ' Error: ', cvsTrainingError:6:2,
                ' Loss: ', cvsTrainingLoss:6:2,
                ' Runtime: ', epochRunTime:5:2, 's',
                ' Fwd: ', (FNN.ForwardTime * 24 * 60 * 60):3:2, 's',
                ' Back: ', (FNN.BackwardTime * 24 * 60 * 60):3:2, 's'
              );
            end;

          // Save Neural Network every 100 steps
          if (i > 0) and (i mod 100 = 0) then
            begin
              ForceDirectories('./backup');
              FNN.SaveToFile('./backup/saved.checkpoint.nn');
              FNN.SaveToFile('./backup/saved.' + i.ToString + '.nn');
            end;

          // Check if network accuracy reached desired accuracy
          if (i > 0) and (i mod 10 = 0) and (cvsTrainingError <= ATargetError) then
            begin
              WriteLn('**** Target error of ', ATargetError:0:2,
                ' reached.');
              ForceDirectories('./backup');
              FNN.SaveToFile('./backup/saved.target.nn');
              Exit;
            end;
        end;

      // ---- Validate ---------------------------------------------------------
      WriteLn('**** Validating ...');

      FGlobalErrorSum := 0;
      FGlobalTotalLoss := 0;
      FGlobalHits := 0;
      FGlobalMisses := 0;

      FWorkingVolumes := FValidationVolumes;
      ProcThreadPool.DoParallel(@ThreadTestProc, 0, FThreadNN.Count - 1, nil,
        FThreadNN.Count);

      cvsValidationRate := FGlobalHits / FWorkingVolumes.Count;
      cvsValidationLoss := FGlobalTotalLoss / FWorkingVolumes.Count;
      cvsValidationError := FGlobalErrorSum / FWorkingVolumes.Count;

      // ---- Test -------------------------------------------------------------
      if (Epoch mod 10 = 0) then
        begin
          WriteLn('**** Testing ...');

          FGlobalErrorSum := 0;
          FGlobalTotalLoss := 0;
          FGlobalHits := 0;
          FGlobalMisses := 0;

          FWorkingVolumes := FTestVolumes;
          ProcThreadPool.DoParallel(@ThreadTestProc, 0, FThreadNN.Count - 1, nil,
            FThreadNN.Count);

          cvsTestRate := FGlobalHits / FWorkingVolumes.Count;
          cvsTestLoss := FGlobalTotalLoss / FWorkingVolumes.Count;
          cvsTestError := FGlobalErrorSum / FWorkingVolumes.Count;

          WriteLn(csvFile,
            Epoch, ';',
            Round((Now - mainStartTime) * 24 * 60 * 60), ';',
            currentLearningRate:0:6, ';',
            cvsTrainingRate:0:2, ';',
            cvsTrainingLoss:0:2, ';',
            cvsTrainingError:0:2, ';',
            cvsValidationRate:0:2, ';',
            cvsValidationLoss:0:2, ';',
            cvsValidationError:0:2, ';',
            cvsTestRate:0:2,';',
            cvsTestLoss:0:2,';',
            cvsTestError:0:2
          );
        end
      else
        begin
          WriteLn(csvFile,
            Epoch, ';',
            Round((Now - mainStartTime) * 24 * 60 * 60), ';',
            currentLearningRate:0:6, ';',
            cvsTrainingRate:0:2, ';',
            cvsTrainingLoss:0:2, ';',
            cvsTrainingError:0:2, ';',
            cvsValidationRate:0:2, ';',
            cvsValidationLoss:0:2, ';',
            cvsValidationError:0:2, ';'
          );
        end;

      // Reset protocol file
      CloseFile(csvFile);
      AssignFile(csvFile, protocolFileName);
      Append(csvFile);

      Inc(learningRateCounter);

      // Adjust Learning rate
      if FNNConfig.StaircaseLearning then
        begin
          newLearningRate := currentLearningRate *
            Power(FNNConfig.LearningRateDecay, FNNConfig.StaircaseEpochs);

          if (newLearningRate >= FNNConfig.MinLearningRate) and
             (learningRateCounter >= FNNConfig.StaircaseEpochs) then
            begin
              learningRateCounter := 0;
              currentLearningRate := newLearningRate;

              FThreadNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.ClearInertia();

              WriteLn('** Learning adjusted to ', currentLearningRate:7:5);
            end;
        end
      else
        begin
          if (currentLearningRate * FNNConfig.LearningRateDecay >
              FNNConfig.MinLearningRate) then
            begin
              currentLearningRate *= FNNConfig.LearningRateDecay;

              FThreadNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.ClearInertia();

              WriteLn('** Learning adjusted to:', currentLearningRate:7:5);
            end;
        end;

      // ----

      mainSeconds := Round((Now - mainStartTime) * 24 * 60 * 60);
		  epochSeconds := Round((Now - epochStartTime) * 24 * 60 * 60);
      WriteLn(
        '-- Epoch #', Epoch, ' finished after ', epochSeconds, 's. ',
        'Total runtime: ', mainSeconds, 's'
      );
    end;

  CloseFile(csvFile);
end;

// Set Defaults, Load Images and Create Neural Network.
function TCifarTrainer.Prepare: Boolean;
begin
  WriteLn('**** Initializing ...');

  FNNConfig.SetDefaults;

  LoadImages;
  Result := CreateNeuralNetwork;
end;

// Clean up
procedure TCifarTrainer.Finish;
begin
  WriteLn('**** Finalizing ...');

  FreeImages;
  FreeNeuralNetwork;
end;

// Configure OpenCL device
procedure TCifarTrainer.OpenCL_Probe;
{$IFDEF OpenCL}
var
  platforms: TPlatformNames;
  platformId: cl_platform_id;
  deviceId: cl_device_id;

  i, c: Integer;
{$ENDIF}
begin
{$IFDEF OpenCL}
  if not FileExists('./cai_dot_product.cl') then
    begin
      WriteLn('ERROR: "./cai_dot_product.cl" not found. Please copy the file from the CAI libs folder.');
      Halt;
    end;

  platforms := FOpenCL.PlatformNames;
  if Length(platforms) > 0 then
    begin
      // Select OpenCL Platform
      WriteLn('Select OpenCL Platform:');
      WriteLn;
      for i := Low(platforms) to High(platforms) do
        WriteLn(#9, '[', i, '] ', platforms[i]);
      WriteLn;
      Write('Enter Platform number and press Enter: ');
      try
        ReadLn(i);
        if not (i in [Low(platforms), High(platforms)]) then
          begin
            WriteLn('Error: You need to enter a valid number.');
            Halt;
          end;
      except
        WriteLn('Error: You need to enter a valid number.');
        Halt;
      end;

      // Set selected OpenCL Platform
      platformId := FOpenCL.PlatformIds[i];
      FOpenCL.SetCurrentPlatform(platformId);

      c := FOpenCL.GetDeviceCount();
      if c > 0 then
        begin
          // Select OpenCL device
          WriteLn('Select OpenCL Device:');
          WriteLn;
          for i := 0 to c - 1 do
            WriteLn(#9, '[', i, '] ', FOpenCL.DeviceNames[i]);
          WriteLn;
          Write('Enter Device number and press Enter: ');
          try
            ReadLn(i);
            if not (i in [0, c - 1]) then
              begin
                WriteLn('Error: You need to enter a valid number.');
                Halt;
              end;
          except
            WriteLn('Error: You need to enter a valid number.');
            Halt;
          end;

          DeviceId := FOpenCL.Devices[i];
          FOpenCL.SetCurrentDevice(DeviceId);

          WriteLn;
        end
      else
        begin
          WriteLn('ERROR: No OpenCL Devices found.');
          Halt;
        end;
    end
  else
    begin
      WriteLn('ERROR: No OpenCL Platforms found.');
      Halt;
    end;
{$ENDIF}
end;

// Load CIFAR images into the Target Array
procedure TCifarTrainer.LoadImagesInto(AFileName: String;
	var ATarget: TTinyImagesArray);
var
  Img: TTinyImage;
  cifarFile: TTInyImageFile;
  fs: Int64;
  sz: Integer;
begin
  WriteLn('Loading ', AFileName, ' ...');

  AssignFile(cifarFile, AFileName);
  Reset(cifarFile);

  sz := Length(ATarget);
  fs := FileSize(cifarFile);
  SetLength(ATarget, sz + fs);

  while not EOF(cifarFile) do
    begin
      Read(cifarFile, Img);
      ATarget[sz] := Img;
      Inc(sz);
    end;

  CloseFile(cifarFile);
end;

// Load training, validation and test images
procedure TCifarTrainer.LoadImages;
const
  CIFAR_PATH = '../../../../data/';
	LEARN_FILE_NAME = 'data_batch_%d.bin';
  TEST_FILE_NAME = 'test_batch.bin';
var
  i: Integer;
  FileName: String;
begin
	WriteLn('Loading images ...');

  // Load CIFAR data set #1 to #4 for training
  for i := 1 to 4 do
  	begin
    	FileName := CIFAR_PATH + Format(LEARN_FILE_NAME, [ i ]);
      if FileExists(FileName) then
        LoadImagesInto(FileName, FImagesLearn)
      else
        begin
          WriteLn('ERROR: CIFAR-10 dataset not found.');
          Halt;
        end;
    end;

  // Load CIFAR data set #5 for validation
  i := 5;
  FileName := CIFAR_PATH + Format(LEARN_FILE_NAME, [ i ]);
  if FileExists(FileName) then
    LoadImagesInto(FileName, FImagesValidate)
  else
    begin
      WriteLn('ERROR: CIFAR-10 dataset not found.');
      Halt;
    end;

  // Load CIFAR test data set
	FileName := CIFAR_PATH + TEST_FILE_NAME;
  if FileExists(FileName) then
    LoadImagesInto(FileName, FImagesTest)
  else
    begin
      WriteLn('ERROR: CIFAR-10 dataset not found.');
      Halt;
    end;

  WriteLn('Normalizing images ...');

  // Load training images into NetVolume list and normalize data
  FImageVolumes := TNNetVolumeList.Create;
	LoadIntoNNetVolume(FImageVolumes, FImagesLearn);

  // Load validation images into NetVolume list and normalize data
  FValidationVolumes := TNNetVolumeList.Create;
	LoadIntoNNetVolume(FValidationVolumes, FImagesValidate);

  // Load test images into NetVolume list and normalize data
  FTestVolumes := TNNetVolumeList.Create;
	LoadIntoNNetVolume(FTestVolumes, FImagesTest);

  WriteLn('Images loaded.');
end;

// Load images from an array into the Target Neural Network Volume List
// and normalize image data to be between -2 and +2
procedure TCifarTrainer.LoadIntoNNetVolume(AList: TNNetVolumeList;
  AImages: TTinyImagesArray);
var
  	i: Integer;
begin
  for i := 0 to Length(AImages) - 1 do
  	begin
      // Add image to NetVolume list
    	AList.Add(TNNetVolume.Create);
      LoadTinyImageIntoNNetVolume(AImages[i], AList[i]);

      // Normalize image data to be between -2 and +2
      AList[i].Sub(128);
      AList[i].Divi(64);
    end;
end;

// Clean up image containers
procedure TCifarTrainer.FreeImages;
begin
	WriteLn('Cleaning up images ...');

  Finalize(FImagesLearn);
  Finalize(FImagesValidate);
  Finalize(FImagesTest);

	FImageVolumes.Free;
  FValidationVolumes.Free;
  FTestVolumes.Free;

  WriteLn('Done.');
end;

// Randomly modifies an image when Augmentation is enabled
procedure TCifarTrainer.ModifyInput(var AInput: TNNetVolume);

  procedure Shift(var Input: TNNetVolume);
  var
    Temp: TNNetVolume;
    Amount, i, j: Integer;
  begin
    Temp := TNNetVolume.Create;
    try
      Temp.Copy(Input);
      Amount := Round(Random(1280) * FNNConfig.NoiseLevel) * Input.Depth;
      if (Amount > 0) then
        begin
          // Shift the input image right and down
          Input.ShiftRight(Amount);

          j := 0;
          for i := Input.Size - Amount to Input.Size - 1 do
            begin
              Input.FData[j] := Temp.FData[i];
              Inc(j);
            end;
        end;
    finally
      Temp.Free;
    end;
  end;

  procedure Add(var Input: TNNetVolume);
  begin
    Input.AddAtDepth(0, ((Random(1024) - 512) * FNNConfig.NoiseLevel) / 2560);
    if Input.Depth >= 1 then
      Input.AddAtDepth(1, ((Random(1024) - 512) * FNNConfig.NoiseLevel) / 2560);
    if Input.Depth >= 2 then
      Input.AddAtDepth(2, ((Random(1024) - 512) * FNNConfig.NoiseLevel) / 2560);
  end;

  procedure SaltAndPepper(var Input: TNNetVolume);
  begin
    Input.AddSaltAndPepper(Round(((Input.SizeX * Input.SizeY) div 20) *
      FNNConfig.NoiseLevel));
  end;

var
  rnd: Integer;
begin
  rnd := Random(4);
  case rnd of
    0: // Shift
      Shift(AInput);
    1: // Random "Add"
      Add(AInput);
    2: // Salt and Pepper
      SaltAndPepper(AInput);
    3: // Random "Add" + Salt and Pepper
      begin
        Add(AInput);
        SaltAndPepper(AInput);
      end;
  end;
end;

// Thread callback routine for Training
procedure TCifarTrainer.ThreadTrainProc(Index: PtrInt; Data: Pointer;
  Item: TMultiThreadProcItem);
var
  NN: TNNet;
  blockSize, blockSizeRest, i, hits, misses, imageIndex, outputClass: Integer;
  Input, calculatedOutput, expectedOutput: TNNetVolume;
  errorSum, outputValue, currentLoss, totalLoss: Single;
begin
  blockSize := FNNConfig.BatchSize div FNNConfig.LogicalThreadsNum;
  blockSizeRest := FNNConfig.BatchSize mod FNNConfig.LogicalThreadsNum;
  if (Index < blockSizeRest) then
    Inc(blockSize);

  NN := FThreadNN[Index];
  NN.CopyWeights(FNN);
  NN.ClearTime();

  hits := 0;
  misses := 0;
  totalLoss := 0;
  errorSum := 0;

  Input := TNNetVolume.Create;
  calculatedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  expectedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  try
    for i := 1 to blockSize do
      begin
      	// Pick random image from list
        imageIndex := Random(FImageVolumes.Count);
        Input.Copy(FImageVolumes[imageIndex]);
        Input.Tag := FImageVolumes[imageIndex].Tag;

        // 50% chance to mirror image
        if Random(1000) > 500 then
          Input.FlipX();

        // 25% chance to grayscale the image
        if (Input.Depth > 1) and (Random(1000) > 750) then
          Input.MakeGray(csEncodeRGB);

        if FDoAugmentation then
          ModifyInput(Input);

        // Feed forward
        NN.Compute(Input);
  	    NN.GetOutput(calculatedOutput);

        // Backpropagate
        expectedOutput.SetClassForSoftMax(Input.Tag);
    		NN.Backpropagate(expectedOutput);

        // Calculate error
        outputClass := calculatedOutput.GetClass();
        errorSum += expectedOutput.SumDiff(calculatedOutput);

        // Determine accuracy
        if outputClass = Input.Tag then
          Inc(hits)
        else
          Inc(misses);

        // Calculate loss
        outputValue := calculatedOutput.FData[Input.Tag];
        if (outputValue > 0) then
          currentLoss := -Ln(outputValue)
        else
          begin
            WriteLn('---- Error: Invalid output value: ', outputValue);
            currentLoss := 1;
          end;

        totalLoss += currentLoss;
      end;

    // Update global variables
    EnterCriticalSection(FCritSect);
    try
      FGlobalHits += hits;
      FGlobalMisses += misses;
      FGlobalTotalLoss += totalLoss;
      FGlobalErrorSum += errorSum;
      FNN.ForwardTime := FNN.ForwardTime + NN.ForwardTime;
      FNN.BackwardTime := FNN.BackwardTime + NN.BackwardTime;
    finally
      LeaveCriticalSection(FCritSect);
    end;

{$IFDEF Debug}
    FNN.SumDeltas(NN);
{$ELSE}
    FNN.SumDeltasNoChecks(NN);
{$ENDIF}

    NN.ClearDeltas();
  finally
    Input.Free;
    calculatedOutput.Free;
    expectedOutput.Free;
  end;
end;

// Thread callback routine for Validation and Testing
procedure TCifarTrainer.ThreadTestProc(Index: PtrInt; Data: Pointer;
  Item: TMultiThreadProcItem);
var
  NN: TNNet;
  blockSize, startIndex, endIndex, i, hits, misses, imageIndex, outputClass: Integer;
  Input, calculatedOutput, expectedOutput: TNNetVolume;
  errorSum, outputValue, currentLoss, totalLoss: Single;
begin
  NN := FThreadNN[Index];
  NN.CopyWeights(FNN);
  NN.EnableDropouts(FALSE);
  NN.ClearTime();

  hits := 0;
  misses := 0;
  totalLoss := 0;
  errorSum := 0;

  blockSize := FWorkingVolumes.Count div FNNConfig.LogicalThreadsNum;
  startIndex := blockSize * Index;
  endIndex := blockSize * (Index + 1) - 1;

  Input := TNNetVolume.Create;
  calculatedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  expectedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  try
    for i := startIndex to endIndex - 1 do
      begin
        imageIndex := i;
        Input.Copy(FWorkingVolumes[imageIndex]);
        Input.Tag := FWorkingVolumes[imageIndex].Tag;

        // Feed forward
        NN.Compute(Input);
	      NN.GetOutput(calculatedOutput);

        expectedOutput.SetClassForSoftMax(Input.Tag);

        // Calculate error
        outputClass := calculatedOutput.GetClass();
        errorSum += expectedOutput.SumDiff(calculatedOutput);

        // Determine accuracy
        if outputClass = Input.Tag then
          Inc(hits)
        else
          Inc(misses);

        // Calculate loss
        outputValue := calculatedOutput.FData[Input.Tag];
        if (outputValue > 0) then
          currentLoss := -Ln(outputValue)
        else
          begin
            WriteLn('---- Error: Invalid output value: ', outputValue);
            currentLoss := 1;
          end;

        totalLoss += currentLoss;
      end;
    NN.EnableDropouts(TRUE);

    // Update global variables
    EnterCriticalSection(FCritSect);
    try
      FGlobalHits += hits;
      FGlobalMisses += misses;
      FGlobalTotalLoss += totalLoss;
      FGlobalErrorSum += errorSum;
    finally
      LeaveCriticalSection(FCritSect);
    end;
  finally
    Input.Free;
    calculatedOutput.Free;
    expectedOutput.Free;
  end;
end;

// Create a new Neural Network or load an existing Neural Network save state
function TCifarTrainer.CreateNeuralNetwork: Boolean;
var
  fileName: String;
  Input: TNNetLayer;
  Branches: Array of TNNetLayer;
begin
  Result := TRUE;

  WriteLn('Creating Neural Network ...');

  FNN := TNNet.Create;

  fileName := './backup/saved.checkpoint.nn';
  if HasOption('usenn') then
  	begin
  		fileName := GetOptionValue('usenn');
      if not FileExists(fileName) then
        begin
          WriteLn('Error: "', fileName, '" does not exist.');
          Result := FALSE;
          Exit;
        end;
    end;

  if FileExists(fileName) then
    begin
      // If a saved checkpoint exists, pick up from there.
      WriteLn('Loading saved Neural Network ...');
      FNN.LoadFromFile(fileName);
      FIsNewNetwork := FALSE;
    end
	else
  	begin
      // Otherwise build the Neural Network

      FIsNewNetwork := TRUE;

      // ** Input
      Input := FNN.AddLayer(TNNetInput.Create(FNNConfig.Width, FNNConfig.Height,
  	    FNNConfig.Depth));

      FNN.AddLayer(TNNetConvolutionReLU.Create(FNNConfig.FeaturesNum,
			  FNNConfig.FeatureSize, 0, FNNConfig.Stride));

      SetLength(Branches, 3);
			try
        // Branch1 (default is 5x5 features)
        FNN.AddLayer(TNNetMaxPool.Create(2));
        FNN.AddLayer(TNNetConvolutionReLU.Create(FNNConfig.InnerConvNeuronCount,
			    FNNConfig.FeatureSize, 0, 0));
        FNN.AddLayer(TNNetMaxPool.Create(2));
        Branches[0] := FNN.AddLayer(TNNetConvolutionReLU.Create(
          FNNConfig.InnerConvNeuronCount, FNNConfig.FeatureSize, 0, 0));

        // Branch2 (default is 3x3 features)
        FNN.AddLayerAfter(TNNetConvolutionReLU.Create(FNNConfig.FeaturesNum,
			    FNNConfig.FeatureSize - 2, 0, 0), Input);
        FNN.AddLayer(TNNetMaxPool.Create(2));
        FNN.AddLayer(TNNetConvolutionReLU.Create(FNNConfig.InnerConvNeuronCount,
			    FNNConfig.FeatureSize - 2, 0, 0));
        FNN.AddLayer(TNNetMaxPool.Create(2));
        Branches[1] := FNN.AddLayer(TNNetConvolutionReLU.Create(
          FNNConfig.InnerConvNeuronCount, FNNConfig.FeatureSize - 2, 0, 0));

        // Branch3 (default is 7x7 features)
        FNN.AddLayerAfter(TNNetConvolutionReLU.Create(FNNConfig.FeaturesNum,
			    FNNConfig.FeatureSize + 2, 0, 0), Input);
        FNN.AddLayer(TNNetMaxPool.Create(2));
        FNN.AddLayer(TNNetConvolutionReLU.Create(FNNConfig.InnerConvNeuronCount,
          FNNConfig.FeatureSize + 2, 0, 0));
        FNN.AddLayer(TNNetMaxPool.Create(2));
        Branches[2] := FNN.AddLayer(TNNetConvolutionReLU.Create(
          FNNConfig.InnerConvNeuronCount, FNNConfig.FeatureSize + 2, 0, 0));

        // Concats both branches so the NN has only one end.
        FNN.AddLayer(TNNetConcat.Create([Branches[0], Branches[1], Branches[2]]));

        FNN.AddLayer(TNNetFullConnectReLU.Create(
			    FNNConfig.InnerFullyConnectedNeurons));
        if FNNConfig.Dropout > 0 then
          FNN.AddLayer(TNNetDropout.Create(FNNConfig.Dropout));

        // ** Fully Connected with SoftMax
        FNN.AddLayer(TNNetFullConnectReLU.Create(FNNConfig.NumClasses));
        FNN.AddLayer(TNNetSoftMax.Create());
      finally
        Finalize(Branches);
      end;
    end;

  if FNNConfig.ApplyL2DecayToAllLayers then
    FNN.SetL2Decay(FNNConfig.L2Decay)
  else
	  FNN.SetL2DecayToConvolutionalLayers(FNNConfig.L2Decay);

  FNN.SetLearningRate(FNNConfig.LearningRate, FNNConfig.Inertia);

  FThreadNN := TNNetDataParallelism.Create(FNN, FNNConfig.LogicalThreadsNum);
  FThreadNN.SetLearningRate(FNNConfig.LearningRate, FNNConfig.Inertia);
  FThreadNN.SetBatchUpdate(TRUE);
  FThreadNN.SetL2DecayToConvolutionalLayers(0);
{$IFDEF OpenCL}
  if FUseOpenCL then
    FThreadNN.EnableOpenCL(FOpenCL.CurrentPlatform, FOpenCL.CurrentDevice);
{$ENDIF}

  ProcThreadPool.MaxThreadCount := FNNConfig.PhysicalThreadsNum;

  WriteLn('Neural Network created.');

{$IFDEF DEBUG}
	FNN.DebugWeights();
	FNN.DebugStructure();
{$ENDIF}
end;

// Clean up Neural Network
procedure TCifarTrainer.FreeNeuralNetwork;
begin
	WriteLn('Cleaning up Neural Network ...');

  FNN.Free;
  FThreadNN.Free;;

  WriteLn('Done.');
end;

// Runs the main program
procedure TCifarTrainer.DoRun;
var
  ErrorMsg: String;
begin
  // Quick check parameters
  ErrorMsg := CheckOptions('h', ['help', 'noopencl', 'usenn:']);
  if ErrorMsg <> '' then
    begin
      ShowException(Exception.Create(ErrorMsg));
      Terminate;
      Exit;
    end;

  // Parse parameters
  if HasOption('h', 'help') then
    begin
      WriteHelp;
      Terminate;
      Exit;
    end;

{$IFDEF OpenCL}
  FUseOpenCL := not HasOption('noopencl');

  FOpenCL := TEasyOpenCLV.Create();
  try
    if FUseOpenCL then
      OpenCL_Probe;
{$ENDIF}

  InitCriticalSection(FCritSect);
  try
    if Prepare then
      try
        Train(2000, 0.0);
      finally
        Finish;
      end;
  finally
    DoneCriticalSection(FCritSect);
  end;

{$IFDEF OpenCL}
  finally
    FOpenCL.Free;
  end;
{$ENDIF}

{$IFDEF Debug}
  WriteLn('Program ended. Press Enter to exit.');
  ReadLn;
{$ENDIF}

  // Stop program loop
  Terminate;
end;

// Constructor
constructor TCifarTrainer.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException := True;
end;

// Destructor
destructor TCifarTrainer.Destroy;
begin
  inherited Destroy;
end;

// Output possible program parameters
procedure TCifarTrainer.WriteHelp;
begin
  { add your help code here }
  WriteLn('Usage: ', ExeName, ' -h');
end;

var
  Application: TCifarTrainer;
begin
	// Memory leak tracing is only used in Debug mode
{$IFDEF Debug}
  {$IF DECLARED(UseHeapTrace)}
    if FileExists('heap.trc') then
      DeleteFile('heap.trc');
    SetHeapTraceOutput('heap.trc');
  {$IFEND}
{$ENDIF}

	Randomize;

  Application := TCifarTrainer.Create(nil);
  Application.Title := 'Cifar Trainer';
  Application.Run;
  Application.Free;
end.

