# CIFAR-10 Dataset

You can download the CIFAR-10 Dataset from the following address:

[https://www.cs.toronto.edu/~kriz/cifar.html](https://www.cs.toronto.edu/~kriz/cifar.html)

Use the **CIFAR-10 binary version (suitable for C programs)**!