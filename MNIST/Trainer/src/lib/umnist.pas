unit UMNIST;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,

  UVolume;

type
  TMNISTLabelHeader = record
    Magic: Integer;
    NumItems: Integer;
  end;

  TMNISTDataHeader = record
    Magic: Integer;
    NumItems: Integer;
    Rows: Integer;
    Cols: Integer;
  end;

  TMNISTEntry = record
    Rows: Integer;
    Cols: Integer;
    Lbl: Byte;
    Pixels: Array of Byte;
  end;

  TMNISTData = Array of TMNISTEntry;

function LoadMNISTDataset(APath, ADatasetName: String; var AData: TMNISTData): Boolean;
procedure FreeMNISTDataset(var AData: TMNISTData);

// Loads a MNIST image into TNNetVolume.
procedure LoadMNISTImageIntoNNetVolume(var MI: TMNISTEntry; var Vol: TNNetVolume);

implementation

function LoadMNISTDataset(APath, ADatasetName: String; var AData: TMNISTData): Boolean;
var
  hndLabels, hndData, i, r, sz: Integer;
  fileNameLabels, fileNameData: String;
  labelsHeader: TMNISTLabelHeader;
  dataHeader: TMNISTDataHeader;
  mnistEntry: TMNISTEntry;
  labelsData: Array of Byte;
begin
  Result := FALSE;

  fileNameLabels := APath + ADatasetName + '-labels.idx1-ubyte';
  fileNameData := APath + ADatasetName + '-images.idx3-ubyte';

  if not FileExists(fileNameLabels) or not FileExists(fileNameData) then
    Exit;

  hndLabels := FileOpen(fileNameLabels, fmOpenRead);
  hndData := FileOpen(fileNameData, fmOpenRead);
  try
    if (hndLabels < 0) or (hndData < 0) then
      Exit;

    sz := SizeOf(TMNISTLabelHeader);
    r := FileRead(hndLabels, labelsHeader, sz);
    if (r <> sz) then
      Exit;

    sz := SizeOf(TMNISTDataHeader);
    r := FileRead(hndData, dataHeader, sz);
    if (r <> sz) then
      Exit;

    labelsHeader.Magic := SwapEndian(labelsHeader.Magic);
    labelsHeader.NumItems := SwapEndian(labelsHeader.NumItems);
    dataHeader.Magic := SwapEndian(dataHeader.Magic);
    dataHeader.NumItems := SwapEndian(dataHeader.NumItems);
    dataHeader.Rows := SwapEndian(dataHeader.Rows);
    dataHeader.Cols := SwapEndian(dataHeader.Cols);

    if (labelsHeader.NumItems <> dataHeader.NumItems) then
      Exit;

    SetLength(labelsData, labelsHeader.NumItems);
    sz := SizeOf(Byte) * labelsHeader.NumItems;
    r := FileRead(hndLabels, labelsData[0], sz);
    if (r <> sz) then
      Exit;

    SetLength(AData, labelsHeader.NumItems);
    for i := 0 to labelsHeader.NumItems - 1 do
      begin
        mnistEntry.Lbl := labelsData[i];
        mnistEntry.Rows := dataHeader.Rows;
        mnistEntry.Cols := dataHeader.Cols;

        sz := dataHeader.Rows * dataHeader.Cols;
        SetLength(mnistEntry.Pixels, sz);

        sz := sz * SizeOf(Byte);
        r := FileRead(hndData, mnistEntry.Pixels[0], sz);
        if (r <> sz) then
          begin
            Finalize(mnistEntry.Pixels);
            FreeMNISTDataset(AData);
            Exit;
          end;

        AData[i] := mnistEntry;
      end;
  finally
    Finalize(labelsData);

    FileClose(hndLabels);
    FileClose(hndData);
  end;

  Result := TRUE;
end;

procedure FreeMNISTDataset(var AData: TMNISTData);
var
  i: Integer;
begin
  for i := Low(AData) to High(AData) do
    Finalize(AData[i].Pixels);
  Finalize(AData);
end;

procedure LoadMNISTImageIntoNNetVolume(var MI: TMNISTEntry; var Vol: TNNetVolume);
var
  x, y: Integer;
begin
  Vol.Resize(MI.Rows, MI.Cols, 1);
  for y := 0 to MI.Rows - 1 do
    for x := 0 to MI.Cols - 1 do
      Vol[x, y, 0] := MI.Pixels[x + (MI.Rows * y)];
  Vol.Tag := MI.Lbl;
end;

end.

