program mnist_trainer;

{$MODE OBJFPC}{$H+}
{$MODESWITCH ADVANCEDRECORDS}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, Classes, SysUtils, CustApp, MTProcs, Math, Graphics,
  { you can add units after this }
  UCIFAR10, UMNIST, UConvolutionNeuralNetwork, UVolume
  {$IFDEF OpenCL}
  , UEasyOpenCL, CL
  {$ENDIF}
  ;

type
  { TNeuralNetworkConfig }
  TNeuralNetworkConfig = record
    Width: Integer;
    Height: Integer;
    Depth: Integer;
		FeaturesNum: Integer;
    FeatureSize: Integer;
    Stride: Integer;
    MaxPool: Integer;
    NumClasses: Integer;

    LogicalThreadsNum: Integer;
    PhysicalThreadsNum: Integer;
    BatchSize: Integer;

    Dropout: Single;
    LearningRate: Single;
    Inertia: Single;
    MinLearningRate: Single;
    LearningRateDecay: Single;

    StaircaseLearning: Boolean;
    StaircaseEpochs: Integer;

    NoiseLevel: Single;

    L2Decay: Single;

    ApplyL2DecayToAllLayers: Boolean;

    InnerConvNeuronCount: Integer;
    InnerConvFeatureSize: Integer;
    InnerConvPadding: Integer;
    InnerFullyConnectedNeurons: Integer;

    procedure SetDefaults;
  end;

  { TMnistTrainer }
  TMnistTrainer = class(TCustomApplication)
  private
  	FImagesLearn: TMNISTData;
    FImagesTest: TMNISTData;

    FImageVolumes: TNNetVolumeList;
    FTestVolumes: TNNetVolumeList;
    FWorkingVolumes: TNNetVolumeList;

    FNN: TNNet;
    FNNConfig: TNeuralNetworkConfig;

{$IFDEF OpenCL}
    FUseOpenCL: Boolean;
    FOpenCL: TEasyOpenCLV;
{$ENDIF}

    FThreadNN: TNNetDataParallelism;
    FCritSect: TRTLCriticalSection;

    FGlobalHits: Integer;
    FGlobalMisses: Integer;
    FGlobalErrorSum: Single;
    FGlobalTotalLoss: Single;

    FImagesCounter: Integer;
    FInputImagesList: TThreadList;

    FDoAugmentation: Boolean;
    FIsNewNetwork: Boolean;
    FDoSaveImages: Boolean;

    procedure Train(AEpochs: Integer; ATargetError: Single);

    function Prepare: Boolean;
    procedure Finish;

    procedure OpenCL_Probe;

    procedure LoadImages;
    procedure LoadIntoNNetVolume(AList: TNNetVolumeList; AImages: TMNISTData);
    procedure FreeImages;
    procedure ClearInputImagesList;

    procedure ModifyInput(var AInput: TNNetVolume);
    procedure SaveInputImages;

    procedure ThreadTrainProc(Index: PtrInt; Data: Pointer;
      Item: TMultiThreadProcItem);
    procedure ThreadTestProc(Index: PtrInt; Data: Pointer;
      Item: TMultiThreadProcItem);

    function CreateNeuralNetwork: Boolean;
    procedure FreeNeuralNetwork;
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TNeuralNetworkConfig }

// Set Neural Network configuration defaults
procedure TNeuralNetworkConfig.SetDefaults;
begin
  Width := 28;
  Height := 28;
  Depth := 1;
	FeaturesNum := 28;
  FeatureSize := 5;
  Stride := 1;
  MaxPool := 4;
  NumClasses := 10;

  LogicalThreadsNum := 4;
  PhysicalThreadsNum := 4;
  BatchSize := 128;

  Dropout := 0.0;
  LearningRate := 0.001;
  Inertia := 0.99;

  MinLearningRate := 0.00001;
  LearningRateDecay := 0.99;

  StaircaseLearning := TRUE;
  StaircaseEpochs := 1;

  NoiseLevel := 0.1;

  L2Decay := 0.001;
  ApplyL2DecayToAllLayers := TRUE;

  InnerConvNeuronCount := 64;
  InnerConvFeatureSize := 3;
  InnerConvPadding := 1;
  InnerFullyConnectedNeurons := 32;
end;

{ TMnistTrainer }

// Main Training and Test routine
procedure TMnistTrainer.Train(AEpochs: Integer; ATargetError: Single);
var
	i, Epoch, learningRateCounter: Integer;
  mainStartTime, epochStartTime, epochRunTime: Double;
  currentRate, currentLearningRate, newLearningRate,
  cvsTrainingRate, cvsTrainingLoss, cvsTrainingError,
  mainSeconds, epochSeconds, cvsTestRate, cvsTestLoss, cvsTestError: Single;
  csvFile: TextFile;
  protocolFileName: String;
begin
  ForceDirectories('./backup');
  protocolFileName := './backup/protocol.csv';
  AssignFile(csvFile, protocolFileName);
  if FIsNewNetwork then
    begin
      Rewrite(csvFile);
      WriteLn(csvFile,
        'Epoch;Time;Learning Rate;',
        'Training Accuracy;Training Loss;Training Error;',
        'Test Accuracy;Test Loss;Test Error'
      );
    end
  else
    Append(csvFile);

  FDoAugmentation := FALSE;

  FImagesCounter := 0;
  currentLearningRate := FNNConfig.LearningRate;
  learningRateCounter := 0;
  currentRate := 0;
  cvsTrainingRate := 0;
  cvsTrainingError := 0;
  cvsTrainingLoss := 0;
  cvsTestRate := 0;
  cvsTestLoss := 0;
  cvsTestError := 0;

  mainStartTime := Now;

  for Epoch := 1 to AEpochs do
    begin
			epochStartTime := Now;

      // ---- Learn ------------------------------------------------------------
      WriteLn('**** Learning ...');
      for i := 1 to FImageVolumes.Count div FNNConfig.BatchSize do
        begin
          FGlobalErrorSum := 0;
          FGlobalTotalLoss := 0;
          FGlobalHits := 0;
          FGlobalMisses := 0;

          currentRate := 0;
          cvsTrainingRate := 0;
          cvsTrainingError := 0;
          cvsTrainingLoss := 0;

          FNN.ClearTime();
          ProcThreadPool.DoParallel(@ThreadTrainProc, 0, FThreadNN.Count - 1,
            nil, FThreadNN.Count);
          FNN.UpdateWeights();
          FNN.ComputeL2Decay();

          if (FGlobalHits > 0) then
            begin
              currentRate := FGlobalHits / (FGlobalHits + FGlobalMisses);
              cvsTrainingRate := currentRate;
              cvsTrainingError := FGlobalErrorSum / (FGlobalHits + FGlobalMisses);
              cvsTrainingLoss := FGlobalTotalLoss / (FGlobalHits + FGlobalMisses);

              if cvsTestRate > 0 then
                begin
                  // Enable augmentation if the NN is more than 10% overfit.
                  FDoAugmentation := cvsTrainingRate > cvsTestRate + 0.1;
                end;
            end;

          // Output some statistics every 10 steps
          if (FGlobalHits > 0) and (i mod 10 = 0) then
            begin
              epochRunTime := (Now - epochStartTime) * 24 * 60 * 60;

              WriteLn(
                Epoch:2, i:6,
                ' Accuracy: ', (cvsTrainingRate * 100):5:2, '% ',
                ' Error: ', cvsTrainingError:6:3,
                ' Loss: ', cvsTrainingLoss:6:3,
                ' Runtime: ', epochRunTime:5:2, 's',
                ' Fwd: ', (FNN.ForwardTime * 24 * 60 * 60):3:2, 's',
                ' Back: ', (FNN.BackwardTime * 24 * 60 * 60):3:2, 's'
              );
            end;

          // Save Neural Network every 100 steps
          if (i > 0) and (i mod 100 = 0) then
            begin
              ForceDirectories('./backup');
              FNN.SaveToFile('./backup/saved.checkpoint.nn');
              FNN.SaveToFile('./backup/saved.' + i.ToString + '.nn');
            end;
        end;

      // ---- Test -------------------------------------------------------------
      WriteLn('**** Testing ...');

      FGlobalErrorSum := 0;
      FGlobalTotalLoss := 0;
      FGlobalHits := 0;
      FGlobalMisses := 0;

      FWorkingVolumes := FTestVolumes;
      ProcThreadPool.DoParallel(@ThreadTestProc, 0, FThreadNN.Count - 1, nil,
        FThreadNN.Count);

      cvsTestRate := FGlobalHits / FWorkingVolumes.Count;
      cvsTestError := FGlobalErrorSum / FWorkingVolumes.Count;
      cvsTestLoss := FGlobalTotalLoss / FWorkingVolumes.Count;

      epochRunTime := (Now - epochStartTime) * 24 * 60 * 60;

      WriteLn(
        Epoch:2,
        ' Test Accuracy: ', (cvsTestRate * 100):5:2, '% ',
        ' Test Error: ', cvsTestError:6:3,
        ' Test Loss: ', cvsTestLoss:6:3,
        ' Runtime: ', epochRunTime:5:2, 's'
      );

      WriteLn(csvFile,
        Epoch, ';',
        Round((Now - mainStartTime) * 24 * 60 * 60), ';',
        currentLearningRate:0:6, ';',
        cvsTrainingRate:0:2, ';',
        cvsTrainingLoss:0:3, ';',
        cvsTrainingError:0:3, ';',
        cvsTestRate:0:2,';',
        cvsTestLoss:0:3,';',
        cvsTestError:0:3
      );

      // Check if network accuracy reached desired accuracy
      if (cvsTestError <= ATargetError) then
        begin
          mainSeconds := (Now - mainStartTime) * 24 * 60 * 60;

          WriteLn('**** Test target error of ', ATargetError:0:3,
            ' reached after ', mainSeconds:5:2, 's.');
          ForceDirectories('./backup');
          FNN.SaveToFile('./backup/saved.target.nn');

          Break;
        end;

      // Reset protocol file
      CloseFile(csvFile);
      AssignFile(csvFile, protocolFileName);
      Append(csvFile);

      Inc(learningRateCounter);

      // Adjust Learning rate
      if FNNConfig.StaircaseLearning then
        begin
          newLearningRate := currentLearningRate *
            Power(FNNConfig.LearningRateDecay, FNNConfig.StaircaseEpochs);

          if (newLearningRate >= FNNConfig.MinLearningRate) and
             (learningRateCounter >= FNNConfig.StaircaseEpochs) then
            begin
              learningRateCounter := 0;
              currentLearningRate := newLearningRate;

              FThreadNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.ClearInertia();

              WriteLn('** Learning adjusted to ', currentLearningRate:7:5);
            end;
        end
      else
        begin
          if (currentLearningRate * FNNConfig.LearningRateDecay >
              FNNConfig.MinLearningRate) then
            begin
              currentLearningRate *= FNNConfig.LearningRateDecay;

              FThreadNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.SetLearningRate(currentLearningRate, FNNConfig.Inertia);
              FNN.ClearInertia();

              WriteLn('** Learning adjusted to ', currentLearningRate:7:5);
            end;
        end;

      // ----

      mainSeconds := (Now - mainStartTime) * 24 * 60 * 60;
		  epochSeconds := (Now - epochStartTime) * 24 * 60 * 60;
      WriteLn(
        '-- Epoch #', Epoch, ' finished after ', epochSeconds:5:2, 's. ',
        'Total runtime: ', mainSeconds:5:2, 's'
      );

      if FDoSaveImages then
        begin
          WriteLn('-- Saving Images ...');
          SaveInputImages;
        end;
    end;

  CloseFile(csvFile);
end;

// Set Defaults, Load Images and Create Neural Network.
function TMnistTrainer.Prepare: Boolean;
begin
  WriteLn('**** Initializing ...');

  FNNConfig.SetDefaults;

  LoadImages;
  Result := CreateNeuralNetwork;
end;

// Clean up
procedure TMnistTrainer.Finish;
begin
  WriteLn('**** Finalizing ...');

  FreeImages;
  FreeNeuralNetwork;
end;

// Configure OpenCL device
procedure TMnistTrainer.OpenCL_Probe;
{$IFDEF OpenCL}
var
  platforms: TPlatformNames;
  platformId: cl_platform_id;
  deviceId: cl_device_id;

  i, c: Integer;
{$ENDIF}
begin
{$IFDEF OpenCL}
  if not FileExists('./cai_dot_product.cl') then
    begin
      WriteLn('ERROR: "./cai_dot_product.cl" not found. Please copy the file from the CAI libs folder.');
      Halt;
    end;

  platforms := FOpenCL.PlatformNames;
  if Length(platforms) > 0 then
    begin
      // Select OpenCL Platform
      WriteLn('Select OpenCL Platform:');
      WriteLn;
      for i := Low(platforms) to High(platforms) do
        WriteLn(#9, '[', i, '] ', platforms[i]);
      WriteLn;
      Write('Enter Platform number and press Enter: ');
      try
        ReadLn(i);
        if not (i in [Low(platforms), High(platforms)]) then
          begin
            WriteLn('Error: You need to enter a valid number.');
            Halt;
          end;
      except
        WriteLn('Error: You need to enter a valid number.');
        Halt;
      end;

      // Set selected OpenCL Platform
      platformId := FOpenCL.PlatformIds[i];
      FOpenCL.SetCurrentPlatform(platformId);

      c := FOpenCL.GetDeviceCount();
      if c > 0 then
        begin
          // Select OpenCL device
          WriteLn('Select OpenCL Device:');
          WriteLn;
          for i := 0 to c - 1 do
            WriteLn(#9, '[', i, '] ', FOpenCL.DeviceNames[i]);
          WriteLn;
          Write('Enter Device number and press Enter: ');
          try
            ReadLn(i);
            if not (i in [0, c - 1]) then
              begin
                WriteLn('Error: You need to enter a valid number.');
                Halt;
              end;
          except
            WriteLn('Error: You need to enter a valid number.');
            Halt;
          end;

          DeviceId := FOpenCL.Devices[i];
          FOpenCL.SetCurrentDevice(DeviceId);

          WriteLn;
        end
      else
        begin
          WriteLn('ERROR: No OpenCL Devices found.');
          Halt;
        end;
    end
  else
    begin
      WriteLn('ERROR: No OpenCL Platforms found.');
      Halt;
    end;
{$ENDIF}
end;

// Load training and test images
procedure TMnistTrainer.LoadImages;
const
  DATA_PATH = '../../../../data/';
	LEARN_SET_NAME = 'train';
  TEST_SET_NAME = 't10k';
var
  i: Integer;
  FileName: String;
begin
	WriteLn('Loading dataset ...');

  // Load MNIST data set for training
  if not LoadMNISTDataset(DATA_PATH, LEARN_SET_NAME, FImagesLearn) then
    begin
      WriteLn('ERROR: Could not load MNIST dataset!');
      Halt;
    end;

  // Load MNIST test data set
  if not LoadMNISTDataset(DATA_PATH, TEST_SET_NAME, FImagesTest) then
    begin
      WriteLn('ERROR: Could not load MNIST dataset!');
      Halt;
    end;

  WriteLn('Normalizing images ...');

  // Load training images into NetVolume list and normalize data
  FImageVolumes := TNNetVolumeList.Create;
	LoadIntoNNetVolume(FImageVolumes, FImagesLearn);

  // Load test images into NetVolume list and normalize data
  FTestVolumes := TNNetVolumeList.Create;
	LoadIntoNNetVolume(FTestVolumes, FImagesTest);

  WriteLn('Images loaded.');
end;

// Load images from an array into the Target Neural Network Volume List
// and normalize image data to be between -2 and +2
procedure TMnistTrainer.LoadIntoNNetVolume(AList: TNNetVolumeList;
  AImages: TMNISTData);
var
  i: Integer;
  Vol: TNNetVolume;
begin
  for i := 0 to Length(AImages) - 1 do
  	begin
      // Add image to NetVolume list
      Vol := TNNetVolume.Create;
      LoadMNISTImageIntoNNetVolume(AImages[i], Vol);

      // Normalize image data to be between -2 and +2
      Vol.Sub(128);
      Vol.Divi(64);

      AList.Add(Vol);
    end;
end;

// Clean up image containers
procedure TMnistTrainer.FreeImages;
begin
	WriteLn('Cleaning up images ...');

  Finalize(FImagesLearn);
  Finalize(FImagesTest);

	FImageVolumes.Free;
  FTestVolumes.Free;

  WriteLn('Done.');
end;

// Clean up stored input images
procedure TMnistTrainer.ClearInputImagesList;
var
  l: TList;
  i: Integer;
  Input: TNNetVolume;
begin
  l := FInputImagesList.LockList;
  try
    for i := 0 to l.Count - 1 do
      begin
        Input := TNNetVolume(l[i]);
        Input.Free;
      end;
    l.Clear;
  finally
    FInputImagesList.UnlockList;
  end;
end;

// Randomly modifies an Input image
procedure TMnistTrainer.ModifyInput(var AInput: TNNetVolume);

  procedure Add(var Input: TNNetVolume);
  var
    x, y, w, h: Integer;
    i: Integer;
    v: Single;
  begin
    w := Input.SizeX - 1;
    h := Input.SizeY - 1;
    v := ((Random(1024) - 512) * FNNConfig.NoiseLevel) / 2560;

    for y := 0 to h do
      for x := 0 to w do
        begin
          i := Input.GetRawPos(x, y, 0);
          if (Input.FData[i] + v >= -2.0) and
             (Input.FData[i] + v <= 2.0) then
            Input.FData[i] += v;
        end;
  end;

  procedure SaltAndPepper(var Input: TNNetVolume);
  begin
    Input.AddSaltAndPepper(Round(((Input.SizeX * Input.SizeY) div 20) *
      FNNConfig.NoiseLevel));
  end;

  procedure Dim(var Input: TNNetVolume);
  var
    i: Integer;
    v: Single;
  begin
    repeat
      v := Random;
    until (v > 0.1) and (v < 1.0);

    for i := Low(Input.FData) to High(Input.FData) do
      Input.FData[i] := Input.FData[i] * v;
  end;

  procedure Invert(var Input: TNNetVolume);
  var
    i: Integer;
    b: Byte;
  begin
    for i := Low(Input.FData) to High(Input.FData) do
      begin
        b := Round((Input.FData[i] * 64) + 128) xor 255;
        Input.FData[i] := (b - 128) / 64;
      end;
  end;

var
  rnd: Integer;
begin
  rnd := Random(9);
  case rnd of
    0: // Random Add
      Add(AInput);
    1: // Salt and Pepper
      SaltAndPepper(AInput);
    2: // Invert
      Invert(AInput);
    3: // Dim
      Dim(AInput);
    4: // Random Add + Salt & Pepper
      begin
        Add(AInput);
        SaltAndPepper(AInput);
      end;
    5: // Invert + Random Add
      begin
        Invert(AInput);
        Add(AInput);
      end;
    6: // Invert + Salt & Pepper
      begin
        Invert(AInput);
        SaltAndPepper(AInput);
      end;
    7: // Invert + Add + Salt & Pepper
      begin
        Invert(AInput);
        Add(AInput);
        SaltAndPepper(AInput);
      end;
    8: // Invert + Dim
      begin
        Invert(AInput);
        Dim(AInput);
      end;
  end;
end;

// Saves a batch of images used for training to disk.
procedure TMnistTrainer.SaveInputImages;
var
  Bitmap: TBitmap;
  i, x, y: Integer;
  l: TList;
  Input: TNNetVolume;
  b: Byte;
begin
  ForceDirectories('./images/');
  l := FInputImagesList.LockList;
  try
    WriteLn('Saving ', l.Count, ' images ...');
    for i := 0 to l.Count - 1 do
      begin
        Input := TNNetVolume(l[i]);
        Bitmap := TBitmap.Create;
        try
          Bitmap.SetSize(28, 28);
          Bitmap.PixelFormat := pf24Bit;
          for y := 0 to 27 do
            for x := 0 to 27 do
              begin
                b := Round((Input[x, y, 0] * 64) + 128);
                Bitmap.Canvas.Pixels[x, y] := RGBToColor(b, b, b);
              end;
          Bitmap.SaveToFile('./images/' +
            Input.Tag.ToString + '_' + FImagesCounter.ToString + '.bmp');
          Inc(FImagesCounter);
        finally
          Bitmap.Free;
        end;
      end;
  finally
    ClearInputImagesList;
    FInputImagesList.UnlockList;
  end;
end;

// Thread callback routine for Training
procedure TMnistTrainer.ThreadTrainProc(Index: PtrInt; Data: Pointer;
  Item: TMultiThreadProcItem);
var
  NN: TNNet;
  blockSize, blockSizeRest, i, hits, misses, imageIndex, outputClass: Integer;
  Input, listInput, calculatedOutput, expectedOutput: TNNetVolume;
  errorSum, outputValue, currentLoss, totalLoss: Single;
begin
  blockSize := FNNConfig.BatchSize div FNNConfig.LogicalThreadsNum;
  blockSizeRest := FNNConfig.BatchSize mod FNNConfig.LogicalThreadsNum;
  if (Index < blockSizeRest) then
    Inc(blockSize);

  NN := FThreadNN[Index];
  NN.CopyWeights(FNN);
  NN.ClearTime();

  hits := 0;
  misses := 0;
  totalLoss := 0;
  errorSum := 0;

  Input := TNNetVolume.Create;
  calculatedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  expectedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  try
    for i := 1 to blockSize do
      begin
      	// Pick random image from list
        imageIndex := Random(FImageVolumes.Count);
        Input.Copy(FImageVolumes[imageIndex]);
        Input.Tag := FImageVolumes[imageIndex].Tag;

        // Modify input image
        if (Random(1000) >= 500) then
          ModifyInput(Input);

        // Only save some images
        if FDoSaveImages and (Random(1000) >= 990) then
          begin
            listInput := TNNetVolume.Create;
            listInput.Copy(Input);
            listInput.Tag := Input.Tag;
            FInputImagesList.Add(listInput);
          end;

        // Feed forward
        NN.Compute(Input);
  	    NN.GetOutput(calculatedOutput);

        // Backpropagate
        expectedOutput.SetClassForSoftMax(Input.Tag);
    		NN.Backpropagate(expectedOutput);

        // Calculate error
        outputClass := calculatedOutput.GetClass();
        errorSum += expectedOutput.SumDiff(calculatedOutput);

        // Determine accuracy
        if outputClass = Input.Tag then
          Inc(hits)
        else
          Inc(misses);

        // Calculate loss
        outputValue := calculatedOutput.FData[Input.Tag];
        if (outputValue > 0) then
          currentLoss := -Ln(outputValue)
        else
          begin
            WriteLn('---- Error: Invalid output value: ', outputValue);
            currentLoss := 1;
          end;

        totalLoss += currentLoss;
      end;

    // Update global variables
    EnterCriticalSection(FCritSect);
    try
      FGlobalHits += hits;
      FGlobalMisses += misses;
      FGlobalTotalLoss += totalLoss;
      FGlobalErrorSum += errorSum;
      FNN.ForwardTime := FNN.ForwardTime + NN.ForwardTime;
      FNN.BackwardTime := FNN.BackwardTime + NN.BackwardTime;
    finally
      LeaveCriticalSection(FCritSect);
    end;

{$IFDEF Debug}
    FNN.SumDeltas(NN);
{$ELSE}
    FNN.SumDeltasNoChecks(NN);
{$ENDIF}

    NN.ClearDeltas();
  finally
    Input.Free;
    calculatedOutput.Free;
    expectedOutput.Free;
  end;
end;

// Thread callback routine for Testing
procedure TMnistTrainer.ThreadTestProc(Index: PtrInt; Data: Pointer;
  Item: TMultiThreadProcItem);
var
  NN: TNNet;
  blockSize, startIndex, endIndex, i, hits, misses, imageIndex, outputClass: Integer;
  Input, calculatedOutput, expectedOutput: TNNetVolume;
  errorSum, outputValue, currentLoss, totalLoss: Single;
begin
  NN := FThreadNN[Index];
  NN.CopyWeights(FNN);
  NN.EnableDropouts(FALSE);
  NN.ClearTime();

  hits := 0;
  misses := 0;
  totalLoss := 0;
  errorSum := 0;

  blockSize := FWorkingVolumes.Count div FNNConfig.LogicalThreadsNum;
  startIndex := blockSize * Index;
  endIndex := blockSize * (Index + 1) - 1;

  Input := TNNetVolume.Create;
  calculatedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  expectedOutput := TNNetVolume.Create(FNNConfig.NumClasses, 1, 1);
  try
    for i := startIndex to endIndex - 1 do
      begin
        imageIndex := i;
        Input.Copy(FWorkingVolumes[imageIndex]);
        Input.Tag := FWorkingVolumes[imageIndex].Tag;

        // Feed forward
        NN.Compute(Input);
	      NN.GetOutput(calculatedOutput);

        expectedOutput.SetClassForSoftMax(Input.Tag);

        // Calculate error
        outputClass := calculatedOutput.GetClass();
        errorSum += expectedOutput.SumDiff(calculatedOutput);

        // Determine accuracy
        if outputClass = Input.Tag then
          Inc(hits)
        else
          Inc(misses);

        // Calculate loss
        outputValue := calculatedOutput.FData[Input.Tag];
        if (outputValue > 0) then
          currentLoss := -Ln(outputValue)
        else
          begin
            WriteLn('---- Error: Invalid output value: ', outputValue);
            currentLoss := 1;
          end;

        totalLoss += currentLoss;
      end;
    NN.EnableDropouts(TRUE);

    // Update global variables
    EnterCriticalSection(FCritSect);
    try
      FGlobalHits += hits;
      FGlobalMisses += misses;
      FGlobalTotalLoss += totalLoss;
      FGlobalErrorSum += errorSum;
    finally
      LeaveCriticalSection(FCritSect);
    end;
  finally
    Input.Free;
    calculatedOutput.Free;
    expectedOutput.Free;
  end;
end;

// Create a new Neural Network or load an existing Neural Network save state
function TMnistTrainer.CreateNeuralNetwork: Boolean;
var
  fileName: String;
begin
  Result := TRUE;

  WriteLn('Creating Neural Network ...');

  FNN := TNNet.Create;

  fileName := './backup/saved.checkpoint.nn';
  if HasOption('usenn') then
  	begin
  		fileName := GetOptionValue('usenn');
      if not FileExists(fileName) then
        begin
          WriteLn('Error: "', fileName, '" does not exist.');
          Result := FALSE;
          Exit;
        end;
    end;

  if FileExists(fileName) then
    begin
      // If a saved checkpoint exists, pick up from there.
      WriteLn('Loading saved Neural Network ...');
      FNN.LoadFromFile(fileName);
      FIsNewNetwork := FALSE;
    end
	else
  	begin
      // Otherwise build the Neural Network
      FIsNewNetwork := TRUE;

      // ** Input
      FNN.AddLayer(TNNetInput.Create(FNNConfig.Width, FNNConfig.Height, FNNConfig.Depth));

      FNN.AddLayer(TNNetConvolutionReLU.Create(8, 5, 0, 1));
      FNN.AddLayer(TNNetMaxPool.Create(4));
      FNN.AddLayer(TNNetConvolutionReLU.Create(32, 3, 1, 1));
      FNN.AddLayer(TNNetConvolutionReLU.Create(32, 3, 1, 1));
      FNN.AddLayer(TNNetFullConnectReLU.Create(64));
      FNN.AddLayer(TNNetFullConnectReLU.Create(32));
      FNN.AddLayer(TNNetFullConnectLinear.Create(FNNConfig.NumClasses));
      FNN.AddLayer(TNNetSoftMax.Create());
    end;

  if FNNConfig.ApplyL2DecayToAllLayers then
    FNN.SetL2Decay(FNNConfig.L2Decay)
  else
	  FNN.SetL2DecayToConvolutionalLayers(FNNConfig.L2Decay);

  FNN.SetLearningRate(FNNConfig.LearningRate, FNNConfig.Inertia);

  FThreadNN := TNNetDataParallelism.Create(FNN, FNNConfig.LogicalThreadsNum);
  FThreadNN.SetLearningRate(FNNConfig.LearningRate, FNNConfig.Inertia);
  FThreadNN.SetBatchUpdate(TRUE);
  FThreadNN.SetL2DecayToConvolutionalLayers(0);
{$IFDEF OpenCL}
  if FUseOpenCL then
    FThreadNN.EnableOpenCL(FOpenCL.CurrentPlatform, FOpenCL.CurrentDevice);
{$ENDIF}

  ProcThreadPool.MaxThreadCount := FNNConfig.PhysicalThreadsNum;

  WriteLn('Neural Network created.');

{$IFDEF DEBUG}
	FNN.DebugWeights();
	FNN.DebugStructure();
{$ENDIF}
end;

// Clean up Neural Network
procedure TMnistTrainer.FreeNeuralNetwork;
begin
	WriteLn('Cleaning up Neural Network ...');

  FNN.Free;
  FThreadNN.Free;;

  WriteLn('Done.');
end;

// Runs the main program
procedure TMnistTrainer.DoRun;
var
  ErrorMsg: String;
begin
  // Quick check parameters
  ErrorMsg := CheckOptions('h', ['help', 'saveimg', 'opencl', 'usenn:']);
  if ErrorMsg <> '' then
    begin
      ShowException(Exception.Create(ErrorMsg));
      Terminate;
      Exit;
    end;

  // Parse parameters
  if HasOption('h', 'help') then
    begin
      WriteHelp;
      Terminate;
      Exit;
    end;

  FDoSaveImages := HasOption('saveimg');

{$IFDEF OpenCL}
  FUseOpenCL := HasOption('opencl');

  FOpenCL := TEasyOpenCLV.Create();
  try
    if FUseOpenCL then
      OpenCL_Probe;
{$ENDIF}

  FInputImagesList := TThreadList.Create;
  InitCriticalSection(FCritSect);
  try
    if Prepare then
      try
        Train(2000, 0.05);
      finally
        Finish;
      end;
  finally
    ClearInputImagesList;
    FInputImagesList.Free;
    DoneCriticalSection(FCritSect);
  end;

{$IFDEF OpenCL}
  finally
    FOpenCL.Free;
  end;
{$ENDIF}

{$IFDEF Debug}
  WriteLn('Program ended. Press Enter to exit.');
  ReadLn;
{$ENDIF}

  // Stop program loop
  Terminate;
end;

// Constructor
constructor TMnistTrainer.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException := True;
end;

// Destructor
destructor TMnistTrainer.Destroy;
begin
  inherited Destroy;
end;

// Output possible program parameters
procedure TMnistTrainer.WriteHelp;
begin
  { add your help code here }
  WriteLn('Usage: ', ExeName, ' -h');
end;

var
  Application: TMnistTrainer;
begin
	// Memory leak tracing is only used in Debug mode
{$IFDEF Debug}
  {$IF DECLARED(UseHeapTrace)}
    if FileExists('heap.trc') then
      DeleteFile('heap.trc');
    SetHeapTraceOutput('heap.trc');
  {$IFEND}
{$ENDIF}

	Randomize;

  Application := TMnistTrainer.Create(nil);
  Application.Title := 'MNIST Trainer';
  Application.Run;
  Application.Free;
end.

