unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Spin,

  UConvolutionNeuralNetwork, UVolume;

type
  { TForm_Main }

  TForm_Main = class(TForm)
    Button_Predict: TButton;
    Button_Close: TButton;
    Button_Clear: TButton;
    ColorButton_Pen_Color: TColorButton;
    ColorButton_Background_Color: TColorButton;
    Image_Input: TImage;
    Label_Pen_Width_Caption: TLabel;
    Label_Pen_Color_Caption: TLabel;
    Label_Background_Color_Caption: TLabel;
    Label_Output: TLabel;
    Panel_Tools_Container: TPanel;
    Panel_Buttons_Container: TPanel;
    Panel_Prediction_Container: TPanel;
    Panel_Image_Container: TPanel;
    Panel_Main_Container: TPanel;
    SpinEdit_PenWidth: TSpinEdit;
    procedure Button_ClearClick(Sender: TObject);
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_PredictClick(Sender: TObject);
    procedure ColorButton_Background_ColorColorChanged(Sender: TObject);
    procedure ColorButton_Pen_ColorColorChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Image_InputMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image_InputMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image_InputMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image_InputResize(Sender: TObject);
  private
    FDoDraw: Boolean;
    FPenColor: TColor;
    FBackgroundColor: TColor;

    FNN: TNNet;
  public

  end;

var
  Form_Main: TForm_Main;

implementation

uses
  Math;

{$R *.lfm}

{ TForm_Main }

procedure TForm_Main.Button_ClearClick(Sender: TObject);
var
  w: Integer;
begin
  Label_Output.Caption := EmptyStr;

  Image_Input.Picture.Clear;

  if Image_Input.ClientWidth > Image_Input.ClientHeight then
    w := Image_Input.ClientHeight
  else
    w := Image_Input.ClientWidth;
  Image_Input.Picture.Bitmap.SetSize(w, w);

  with Image_Input.Canvas do
    begin
      Brush.Color := FBackgroundColor;
      FillRect(ClipRect);
    end;
end;

procedure TForm_Main.Button_CloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm_Main.Button_PredictClick(Sender: TObject);
var
  Volume: TNNetVolume;
  W, H, X, Y, nnClass: Integer;
  RGB: LongInt;
  gs: Byte;
  Bitmap: TBitmap;
  nnInput, nnOutput: TNNetVolume;
begin
  W := 28;
  H := 28;
  Volume := TNNetVolume.Create(W, H, 1);
  Bitmap := TBitmap.Create;
  nnInput := TNNetVolume.Create;
  nnOutput := TNNetVolume.Create(10, 1, 1);
  try
    Bitmap.PixelFormat := pf24Bit;
    Bitmap.SetSize(W, H);
    Bitmap.Canvas.StretchDraw(Rect(0, 0, W, H), Image_Input.Picture.Bitmap);

    for Y := 0 to H - 1 do
      for X := 0 to W - 1 do
        begin
          RGB := ColorToRGB(Bitmap.Canvas.Pixels[X, Y]);
          gs := Round(
            (0.299 * Red(RGB)) +
            (0.587 * Green(RGB)) +
            (0.114 * Blue(RGB))
          );
          Volume[X, Y, 0] := (gs - 128) / 64;
        end;

    Volume.Tag := 0;
    nnInput.Copy(Volume);
    FNN.Compute(nnInput);
    FNN.GetOutput(nnOutput);
    nnClass := nnOutput.GetClass();

    Label_Output.Caption := nnClass.ToString;
  finally
    Volume.Free;
    Bitmap.Free;
  end;
end;

procedure TForm_Main.ColorButton_Background_ColorColorChanged(Sender: TObject);
begin
  FBackgroundColor := ColorButton_Background_Color.ButtonColor;
  Button_Clear.Click;
end;

procedure TForm_Main.ColorButton_Pen_ColorColorChanged(Sender: TObject);
begin
  FPenColor := ColorButton_Pen_Color.ButtonColor;
end;

procedure TForm_Main.FormCreate(Sender: TObject);
begin
  FPenColor := clWhite;
  FBackgroundColor := clBlack;
  Button_Clear.Click;

  FNN := TNNet.Create;
end;

procedure TForm_Main.FormDestroy(Sender: TObject);
begin
  FNN.Free;
end;

procedure TForm_Main.FormShow(Sender: TObject);
begin
  if FileExists('./saved.checkpoint.nn') then
    begin
      FNN.LoadFromFile('./saved.checkpoint.nn');
      FNN.EnableDropouts(FALSE);
    end
  else
    begin
      MessageDlg('ERROR: The Neural Network structure file "./saved.checkpoint.nn" ' +
        'could not be found.', mtError, [mbOK], 0);
      Application.Terminate;
    end;
end;

procedure TForm_Main.Image_InputMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  w, h: Integer;
begin
  if Image_Input.ClientWidth > Image_Input.ClientHeight then
    w := Image_Input.ClientHeight
  else
    w := Image_Input.ClientWidth;
  w := w div 2;
  h := w;
  if (X >= (Image_Input.ClientWidth div 2) - w) and
     (Y >= (Image_Input.ClientHeight div 2) - h) then
    FDoDraw := TRUE
  else
    FDoDraw := FALSE;
end;

procedure TForm_Main.Image_InputMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  w, offsX, offsY: Integer;
  pt: TPoint;
begin
  if not FDoDraw then
    Exit;

  if Image_Input.ClientWidth > Image_Input.ClientHeight then
    w := Image_Input.ClientHeight
  else
    w := Image_Input.ClientWidth;
  offsX := Max(0, (Image_Input.ClientWidth div 2) - (w div 2));
  offsY := Max(0, (Image_Input.ClientHeight div 2) - (w div 2));

  pt := Point(X - offsX, Y - offsY);
  w := SpinEdit_PenWidth.Value;
  with Image_Input.Canvas do
    begin
      Pen.Width := 1;
      Pen.Color := FPenColor;
      Brush.Color := FPenColor;
      Ellipse(pt.X - w div 2, pt.Y - w div 2, pt.X + w div 2, pt.Y + w div 2);
    end;
end;

procedure TForm_Main.Image_InputMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDoDraw := FALSE;
end;

procedure TForm_Main.Image_InputResize(Sender: TObject);
begin
  Button_Clear.Click;
end;

end.

